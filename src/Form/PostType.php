<?php

namespace App\Form;

use App\Constant\PostImageConstant;
use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'help' => 'maksymalnie 120 znaków'
            ])
            ->add('content', TextareaType::class, [
                'help' => 'dozwolone tagi HTML: ul, li, ol, p, strong'
            ])
            ->add('image', FileType::class, [
                'help' => 'dozwolone formaty pliku: JPEG, JPG, PNG, GIF, maksymalny rozmiar: 1 MB',
                'mapped' => false,
                'required' => $options['image_required'],
                'constraints' => [
                    new File([
                        'maxSize' => PostImageConstant::MAX_IMAGE_SIZE,
                        'maxSizeMessage' => 'Plik jest zbyt duży. Dozwolony maksymalny rozmiar to 1 MB.',
                        'mimeTypes' => PostImageConstant::ALLOWED_IMAGE_MIME_TYPES,
                        'mimeTypesMessage' => 'Nieprawidłowy format pliku. Dozwolone formaty: JPEG, PNG lub GIF.',
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'image_required' => true,
        ]);
        $resolver->setAllowedTypes('image_required', 'bool');
    }
}
