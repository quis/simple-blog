<?php

namespace App\Command;

use App\Constant\PostImageConstant;
use App\Entity\Post;
use App\Service\PostImageService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreatePostCommand extends Command
{
    private const ARG_TITLE = 'title';
    private const ARG_IMAGE = 'image';
    private const ARG_CONTENT = 'content';

    protected static $defaultName = 'app:create-post';

    public function __construct(
        private PostImageService $postImageService,
        private EntityManagerInterface $entityManager,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Create new blog post.')
            ->addArgument(self::ARG_TITLE, InputArgument::REQUIRED, 'Post title')
            ->addArgument(self::ARG_IMAGE, InputArgument::REQUIRED, 'Path to image file')
            ->addArgument(self::ARG_CONTENT, InputArgument::REQUIRED, 'Post content');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $image = $input->getArgument(self::ARG_IMAGE);
        if (!file_exists($image)) {
            $io->error('File not found.');
            return Command::FAILURE;
        }
        if (filesize($image) > PostImageConstant::MAX_IMAGE_SIZE) {
            $io->error('Max allowed post image size: ' . PostImageConstant::MAX_IMAGE_SIZE . ' bytes.');
            return Command::FAILURE;
        }
        if (!in_array(mime_content_type($image), PostImageConstant::ALLOWED_IMAGE_MIME_TYPES)) {
            $io->error('Allowed image extensions: PNG, JPEG, GIF');
            return Command::FAILURE;
        }

        $post = new Post();
        $post->setTitle($input->getArgument(self::ARG_TITLE));
        $post->setContent($input->getArgument(self::ARG_CONTENT));
        $this->postImageService->saveImageFromFile($post, $image);
        $this->entityManager->persist($post);
        $this->entityManager->flush();

        $io->success('Post created with id: ' . $post->getId());
        return Command::SUCCESS;
    }
}
