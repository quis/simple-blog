<?php

namespace App\ApiEntity;

use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;
use App\Validator as LocalAssert;

class Post
{
    /**
     * @SWG\Property(description="Post title", maxLength=120, example="Some blog post")
     */
    #[Assert\NotBlank, Assert\Length(max: 120)]
    private ?string $title;

    /**
     * @Assert\NotBlank
     * @SWG\Property(description="Post content, allowed HTML tags: ul, li, ol, p, strong", example="<strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet libero eget ex aliquet congue. Cras et ante eu mauris pellentesque bibendum. Maecenas pharetra feugiat orci.")
     */
    #[Assert\NotBlank]
    private ?string $content;

    /**
     * @SWG\Property(description="Post image encoded in base64", example="iVBORw0KGgoAAAANSUhEUgAAAKsAAACnCAIAAADhSDs1AAAAA3NCSVQICAjb4U/gAAAAEHRFWHRTb2Z0d2FyZQBTaHV0dGVyY4LQCQAAAS1JREFUeNrt0gENAAAIwzDAzv37QwehlbCskxSPjQQOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3CAAyRwAA7AATgAB+AAHIADcAAOwAE4AAfgAByAA3AADsABOAAH4AAcgANwAA7AATgAB+AAHIADcAAOwAE4gKMWF0cBzwZppJAAAAAASUVORK5CYII=")
     */
    #[Assert\NotBlank, LocalAssert\ValidBase64Image]
    private ?string $image;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(?string $image): void
    {
        $this->image = $image;
    }
}