<?php

namespace App\Controller\Api;

use App\Constant\PostImageConstant;
use App\Entity\Post as DbPost;
use App\Extractor\PostExtractor;
use App\Repository\PostRepository;
use App\Service\ClearPostHtmlTagsService;
use App\Service\CreateJsonResponseFromErrorsService;
use App\Service\PostImageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\ApiEntity\Post as ApiPost;

#[Route('/post')]
class PostController extends AbstractController
{
    public function __construct(
        private PostImageService $postImageService,
        private ClearPostHtmlTagsService $clearPostHtmlTagsService,
        private CreateJsonResponseFromErrorsService $createJsonResponseFromErrorsService,
        private PostExtractor $postExtractor
    ) {
    }

    /**
     * List all posts with pagination.
     *
     * @SWG\Parameter(
     *     name="page",
     *     in="path",
     *     type="integer",
     *     default=1,
     *     description="Number of page"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the posts.",
     *     @SWG\Schema(
     *          @SWG\Property(property="pageCount", description="All pages count", type="integer", example="5"),
     *          @SWG\Property(property="totalCount", description="Total records count", type="integer", example="5"),
     *          @SWG\Property(
     *              property="list",
     *              type="array",
     *              @SWG\Items(
     *                  ref=@Model(type=DbPost::class)
     *              )
     *          )
     *     )
     * )
     * @param int $page
     * @param PostRepository $postRepository
     * @return JsonResponse
     */
    #[Route('/{page<\d+>?1}', name: 'api_post_index', methods: ['GET'])]
    public function list(
        int $page,
        PostRepository $postRepository
    ): JsonResponse {
        $totalCount = $postRepository->getTotalCount();
        $pageCount = $postRepository->getPageCount($totalCount);
        return new JsonResponse([
            'pageCount' => $pageCount,
            'totalCount' => $totalCount,
            'list' => array_map(fn($post) => $this->postExtractor->extract($post), $postRepository->findByPage($page)),
        ]);
    }

    /**
     * Create new post.
     *
     * @SWG\Parameter(
     *     name="post",
     *     in="body",
     *     @Model(type=ApiPost::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Post created.",
     *     @SWG\Schema(
     *      @SWG\Property(property="id", type="string", format="uuid", example="50c1e975-7a0c-11eb-ae7b-0242ac140002"),
     *      @SWG\Property(property="imageId", type="string", format="uuid", example="50c1e975-7a0c-11eb-ae7b-0242ac140017")
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Invalid params provided.",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="errors",
     *              type="object",
     *              @SWG\Property(
     *                  property="title",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="string",
     *                      example="invalid value"
     *                  )
     *              )
     *          )
     *     )
     * )
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    #[Route('', name: 'api_post_create', methods: ['POST'])]
    public function create(
        Request $request,
        ValidatorInterface $validator
    ): JsonResponse {
        $image = base64_decode($request->get('image', ''));

        $post = new ApiPost();
        $post->setTitle($request->get('title'));
        $post->setContent($request->get('content'));
        $post->setImage($image);
        $errors = $validator->validate($post);

        if (count($errors) > 0) {
            return $this->createJsonResponseFromErrorsService->execute($errors);
        }

        $dbPost = new DbPost();
        $this->postImageService->saveImageFromString($dbPost, $image);
        $imageErrors = $this->validatePostImage($dbPost);
        if (!empty($imageErrors)) {
            return new JsonResponse(['errors' => ['image' => $imageErrors]], Response::HTTP_BAD_REQUEST);
        }
        $dbPost->setTitle($post->getTitle());
        $dbPost->setContent($post->getContent());
        $this->clearPostHtmlTagsService->execute($dbPost);
        $this->getDoctrine()->getManager()->persist($dbPost);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['id' => $dbPost->getId(), 'imageId' => $dbPost->getImageId()], Response::HTTP_CREATED);
    }


    /**
     * Get single post.
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     format="uuid"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Post details.",
     *     @SWG\Schema(
     *          ref=@Model(type=DbPost::class)
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Post not found."
     * )
     * @param string $id
     * @param PostRepository $postRepository
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'api_post_details', methods: ['GET'])]
    public function details(
        string $id,
        PostRepository $postRepository
    ): JsonResponse {
        /** @var DbPost|null $post */
        $post = $postRepository->find($id);
        if (!$post) {
            return new JsonResponse('Not found.', Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse($this->postExtractor->extract($post));
    }

    /**
     * Delete the post from database
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     format="uuid"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Post deleted.",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Post not found."
     * )
     * @param string $id
     * @param PostRepository $postRepository
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'api_post_delete', methods: ['DELETE'])]
    public function delete(
        string $id,
        PostRepository $postRepository
    ): JsonResponse {
        /** @var DbPost|null $post */
        $post = $postRepository->find($id);
        if (!$post) {
            return new JsonResponse('Not found.', Response::HTTP_NOT_FOUND);
        }
        $this->getDoctrine()->getManager()->remove($post);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse('Deleted.');
    }

    private function validatePostImage(DbPost $post): array
    {
        $imagePath = $this->postImageService->getPostImagePath($post);
        $errors = [];
        if (filesize($imagePath) > PostImageConstant::MAX_IMAGE_SIZE) {
            $errors[] = 'Image size is too big. Max allowed size: ' . PostImageConstant::MAX_IMAGE_SIZE . ' bytes';
        } elseif (!in_array(mime_content_type($imagePath), PostImageConstant::ALLOWED_IMAGE_MIME_TYPES)) {
            $errors[] = 'Invalid image MIME type. Allowed MIME types: ' . implode(',',
                    PostImageConstant::ALLOWED_IMAGE_MIME_TYPES);
        }
        if (!empty($errors)) {
            $this->postImageService->deleteImage($post);
        }
        return $errors;
    }
}