<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Service\ClearPostHtmlTagsService;
use App\Service\PostImageService;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[Route('/post')]
class PostController extends AbstractController
{
    public function __construct(
        private ClearPostHtmlTagsService $clearPostHtmlTagsService,
        private PostImageService $postImageService
    ) {
    }

    #[Route('/{page<\d+>?1}', name: 'post_index', methods: ['GET'])]
    public function index(
        ?int $page,
        PostRepository $postRepository
    ): Response {
        $pageCount = $postRepository->getPageCount();
        if ($page > $pageCount && $page !== 1) {
            return $this->redirectToRoute('post_index');
        }
        return $this->render('post/index.html.twig', [
            'page' => $page,
            'pageCount' => $pageCount,
            'posts' => $postRepository->findByPage($page),
        ]);
    }

    #[Route('/new', name: 'post_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request
    ): Response {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post, [
            'image_required' => true
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->clearPostHtmlTagsService->execute($post);
            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();
            $this->postImageService->saveImageFromUploadedFile($post, $image);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'post_show', methods: ['GET'])]
    public function show(
        ?Post $post
    ): Response {
        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    #[Route('/{id}/edit', name: 'post_edit', methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        ?Post $post
    ): Response {
        $form = $this->createForm(PostType::class, $post, [
            'image_required' => false
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->clearPostHtmlTagsService->execute($post);
            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();
            if ($image) {
                // delete old image and save new
                $this->postImageService->deleteImage($post);
                $this->postImageService->saveImageFromUploadedFile($post, $image);
            }
            $post->setUpdatedAt(new DateTimeImmutable());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'post_delete', methods: ['DELETE'])]
    public function delete(
        Request $request,
        Post $post
    ): Response {
        if ($this->isCsrfTokenValid('delete' . $post->getId(), $request->request->get('_token'))) {
            $this->postImageService->deleteImage($post);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_index');
    }
}
