<?php

namespace App\Constant;

abstract class PostImageConstant
{
    public const ALLOWED_IMAGE_MIME_TYPES = [
        'image/png',
        'image/jpeg',
        'image/gif'
    ];
    public const MAX_IMAGE_SIZE = 1048576; // 1 MB in bytes
}