<?php

namespace App\Extractor;

use App\Entity\Post;
use DateTimeInterface;

class PostExtractor
{
    public function extract(Post $post): array
    {
        return [
            'id' => $post->getId(),
            'createdAt' => $post->getCreatedAt()->format(DateTimeInterface::ATOM),
            'updatedAt' => $post->getUpdatedAt()->format(DateTimeInterface::ATOM),
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'imageId' => $post->getImageId()
        ];
    }
}