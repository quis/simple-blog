<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ValidBase64ImageValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidBase64Image) {
            throw new UnexpectedTypeException($constraint, ValidBase64Image::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if ($value === false) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }
    }
}