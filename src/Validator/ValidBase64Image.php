<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ValidBase64Image extends Constraint
{
    public $message = 'Invalid base64 encoded image.';
}