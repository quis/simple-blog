<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class CreateJsonResponseFromErrorsService
{
    public function execute(ConstraintViolationListInterface $errors): JsonResponse
    {
        $errorMessages = [];
        /** @var ConstraintViolationInterface $error */
        foreach ($errors as $error) {
            $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
        }
        return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
    }
}