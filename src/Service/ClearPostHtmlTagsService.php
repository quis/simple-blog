<?php

namespace App\Service;

use App\Entity\Post;

class ClearPostHtmlTagsService
{
    private const ALLOWED_CONTENT_TAGS = ['ul', 'li', 'ol', 'p', 'strong'];

    public function execute(Post $post): void
    {
        $title = $post->getTitle();
        $post->setTitle(strip_tags($title));
        $content = $post->getContent();
        $post->setContent(strip_tags($content, self::ALLOWED_CONTENT_TAGS));
    }
}