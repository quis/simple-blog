<?php

namespace App\Service;

use App\Entity\Post;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Uid\Uuid;

class PostImageService
{
    private const POST_IMAGES_DIR = 'public/images/posts';

    private string $imagesDir;

    public function __construct(KernelInterface $kernel)
    {
        $this->imagesDir = $kernel->getProjectDir() . '/' . self::POST_IMAGES_DIR . '/';
    }

    public function getPostImagePath(Post $post): string
    {
        return $this->imagesDir . $post->getImageId();
    }

    public function deleteImage(Post $post): void
    {
        unlink($this->imagesDir . $post->getImageId());
    }

    public function saveImageFromUploadedFile(Post $post, UploadedFile $image): void
    {
        $imageId = Uuid::v4();
        $image->move($this->imagesDir, $imageId);
        $post->setImageId($imageId);
    }

    public function saveImageFromFile(Post $post, string $filePath): void
    {
        $imageId = Uuid::v4();
        copy($filePath, $this->imagesDir . $imageId);
        $post->setImageId($imageId);
    }

    public function saveImageFromString(Post $post, string $image): void
    {
        $imageId = Uuid::v4();
        file_put_contents($this->imagesDir . $imageId, $image);
        $post->setImageId($imageId);
    }
}