<?php

namespace App\Entity;

use App\Repository\PostRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     * @SWG\Property(description="Post id", format="uuid", example="50c1e975-7a0c-11eb-ae7b-0242ac140002")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=120)
     * @SWG\Property(description="Post title", example="Some post title")
     */
    private string $title;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @SWG\Property(description="Post creation datetime", format="date-time")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @SWG\Property(description="Post last update datetime", format="date-time")
     */
    private DateTimeInterface $updatedAt;

    /**
     * @ORM\Column(type="text")
     * @SWG\Property(description="Post content (with allowed HTML tags: ul, ol, li, p, strong)")
     */
    private string $content;

    /**
     * @ORM\Column(name="image_id", type="string", length=36)
     * @SWG\Property(description="imageId can be used with images path to access image by public url")
     */
    private string $imageId = '';

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->createdAt = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updated_at): self
    {
        $this->updatedAt = $updated_at;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getImageId(): ?string
    {
        return $this->imageId;
    }

    public function setImageId(string $imageId): self
    {
        $this->imageId = $imageId;

        return $this;
    }
}
