# Wymagania do uruchomienia:
* **docker** w wersji `>=20.10.2`
* **docker-compose** w wersji `>=1.25.0`

# Instrukcja uruchomienia:
1. Sklonuj repozytorium.
2. Przejdź do katalogu projektu.
3. Uruchom `docker-compose up -d`
4. Uruchom terminal w dockerze (`docker exec -it blog-php bash`)
5. Uruchom `composer install` aby zainstalować zależności
6. Uruchom `php bin/console doctrine:schema:create` aby utworzyć strukturę bazy danych.
7. Uruchom `symfony serve` aby uruchomić serwer deweloperski PHP
8. Aplikacja jest dostępna pod adresem http://localhost:8000 (port można zmienić przy mapowaniu `docker-compose.yml` jeśli jest taka potrzeba)
9. (Opcjonalnie) Zmień `command: tail -f /dev/null` na `command: symfony serve` pod serwisem `blog-php` w `docker-compose.yml` aby serwer startował automatycznie przy każdym kolejnym uruchomieniu środowiska.

# Serwisy:
* http://localhost:8000 - pod tym adresem działa uruchomiona aplikacja (domyślny port `symfony serve`)
* localhost:3308 - pod tym adresem jest dostępna baza danych MariaDB (port można zmienić przy mapowaniu `docker-compose.yml` jeśli jest taka potrzeba)
* http://localhost:8082 - pod tym adresem dostępny jest interfejs dla użytkownika do bazy danych (Adminer), dane do logowania: dbuser:dbpass, host: db (zmapowany w `docker-compose.yml`), port: 3306, baza danych: blog

# Komendy
**UWAGA** komendy należy uruchamiać tylko wewnątrz środowiska deweloperskiego (`docker exec -it blog-php bash`)
* **app:create-post [tytuł] [ścieżka do obrazka] [treść]** - komenda służy do tworzenia postów w aplikacji
    * obraz musi znajdować się wewnątrz dockera, ale katalog główny projektu jest mapowany, więc wystarczy wrzucić obraz do katalogu projektu, dzięki czemu będzie on dostępny
    * przykład wywołania: `php bin/console "post title" test.png "post content <strong>test</strong>"`

# Dokumentacja API
* Swagger (OpenAPI) dostępny jest pod adresem http://localhost:8000/api/doc.json
* Nelmio GUI do testowania API dostępne jest pod adresem http://localhost:8000/api/doc

# Notatki deweloperskie:
* PHP8 Attributes nie są jeszcze wszędzie wspierane, ale myślę, że sukcesywnie będzie można przenosić kod na nowe rozwiązanie.
* katalog ApiEntity zawiera encje przesyłane przez API, katalog Entity zawiera encje bazodanowe, co pozwala zachować oddzielną logikę dla API i dla aplikacji.
* konfiguracja z klasy `PostImageConstant` powinna trafić do dedykowanego pliku w konfiguracji aplikacji
* aplikacja nie ma lokalizacji, polskie etykiety są na sztywno (można to zmienić, jeśli istnieje takie wymaganie)